# !/usr/bin/ksh
# Written by Lorenzo Ramirez
# Function: to monitor the state of multipath maps and its respective devices and paths

# imports necessary packages
import subprocess
import os
import json
import sys
import shutil

# Nagios States
STATE_ACTIVE = 0
STATE_WARNING = 1
STATE_CRITICAL = 2
STATE_UNKNOWN = 3

# Sample multipath command outputs in python scripts - delete when program is complete
MULTIPATHD_PYTHON = "python multipathd.py show maps json"
PVS_PYTHON = "python pvs.py"


def main():
    # Initial exit_code
    exit_code = None

    # Standard output for multipathd
    multipath_output = ""

    # Initial values for the amount of data and how many are up and down
    map_total = 0
    map_up = 0
    map_down = 0
    failed_map_paths = 0
    failed_map_paths_up = 0
    failed_map_paths_down = 0
    path_group_down = 0
    path_total = 0
    path_up = 0
    path_down = 0

    # empty variables for all failed data
    failed_map = ""
    failed_vg_name = ""

    try:
        # Checks program's existence - Uncheck when program is complete
        # open(MULTIPATHD_PYTHON)
        # open(PVS_PYTHON)

        # Receives python output for the multipath data
        multipathd_json = os.popen(MULTIPATHD_PYTHON).read()
        pvs_json = os.popen(PVS_PYTHON).read()

        # Converts outputs into JSON objects
        multipathd = json.loads(str(multipathd_json))
        pvs = json.loads(str(pvs_json))

        for maps in multipathd['maps']:
            map_name = maps['name']
            maps_dm_st = maps['dm_st']
            map_total += 1
            map_up += 1

            if maps_dm_st != "active":
                map_up -= 1
                map_down += 1
                failed_map = map_name

            for path_groups in maps['path_groups']:
                pg_dm_st = path_groups['dm_st']

                if pg_dm_st == "failed":
                    path_group_down += 1
                    failed_map = map_name

                for path in path_groups['paths']:
                    dm_st = path['dm_st']
                    dev_st = path['dev_st']
                    chk_st = path['chk_st']
                    path_total += 1

                    # Determines the status of the device
                    if dm_st != "active" or dev_st != "running" or chk_st != "ready":
                        failed_map = maps['name']
                        path_down += 1
                    else:
                        path_up += 1

                    # Determines amount of up and down paths in failed map **MAY NOT WORK COMPLETELY. NOT FULLY TESTED**
                    if maps_dm_st != "active":
                        if pg_dm_st == "failed":
                            if dm_st != "active" or dev_st != "running" or chk_st != "ready":
                                failed_map_paths_down += 1
                            else:
                                failed_map_paths_up += 1
                            failed_map_paths = failed_map_paths_up + failed_map_paths_down

        # if no failure, message says how many path_groups there are and how many paths there are, up and down
        if map_up == map_total and path_up == path_total and path_group_down == 0:
            multipath_output = "OK: {m_total} maps - {p_total}/{p_up}/{p_down} All paths up".format(m_total=map_total,
                                                                                                    p_total=path_total,
                                                                                                    p_up=path_up,
                                                                                                    p_down=path_down)
            exit_code = STATE_ACTIVE

        # if a path has failed, open pvs output and looks for corresponding vg_name by searching for the failed map name
        elif map_down > 0 or path_down > 0 or path_group_down > 0:
            for report in pvs['report']:
                for pv in report['pv']:
                    if failed_map in pv['pv_name']:
                        failed_vg_name = pv['vg_name']
            if map_down == map_total or path_down == path_total:
                multipath_output = "CRIT: {m_total} maps - {p_total}/{p_up}/{p_down} {f_vg_name} {f_map_paths}/" \
                                   "{f_map_paths_up}/{f_map_paths_down}".format(m_total=map_total, p_total=path_total,
                                                                                p_up=path_up, p_down=path_down,
                                                                                f_vg_name=failed_vg_name,
                                                                                f_map_paths=failed_map_paths,
                                                                                f_map_paths_up=failed_map_paths_up,
                                                                                f_map_paths_down=failed_map_paths_down)
                exit_code = STATE_CRITICAL
            else:
                multipath_output = "WARN: {m_total} maps - {p_total}/{p_up}/{p_down} {f_vg_name} {f_map_paths}/" \
                                   "{f_map_paths_up}/{f_map_paths_down}".format(m_total=map_total, p_total=path_total,
                                                                                p_up=path_up, p_down=path_down,
                                                                                f_vg_name=failed_vg_name,
                                                                                f_map_paths=failed_map_paths,
                                                                                f_map_paths_up=failed_map_paths_up,
                                                                                f_map_paths_down=failed_map_paths_down)
                exit_code = STATE_WARNING
    except FileNotFoundError as file_err:
        exit_code = STATE_UNKNOWN
        multipath_output = "UNKNOWN: Multipath or PVS not found with {err}".format(err=str(file_err))

    # Prints multipath's path states & exits program with return code
    print(multipath_output)
    sys.exit(exit_code)


if __name__ == "__main__":
    main()
