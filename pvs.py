# Written by Lorenzo Ramirez
# Function: represents the output of pvs command

pvs_string = '''
{
    "report": [
        {
            "pv": [
                {
                    "pv_name": "/dev/mapper/mpathb3", "vg_name": "rhel75-base", 
                    "pv_fmt": "lvm2", "pv_attr": "a--",
                    "pv_size": "44.00g", "pv_free": "4.00m"
                },
                {
                    "pv_name": "/dev/mapper/mpathc1", "vg_name": "sap", 
                    "pv_fmt": "lvm2", "pv_attr": "a--",
                    "pv_size": "<20.00g", "pv_free": "1020.00m"
                },
                {
                    "pv_name": "/dev/mapper/mpathd1", "vg_name": "swap", 
                    "pv_fmt": "lvm2", "pv_attr": "a--",
                    "pv_size": "<34.00g", "pv_free": "<2.00g"
                }
            ]
        }
    ]
}
'''

print(pvs_string)
