# !/usr/bin/ksh
# Written by Lorenzo Ramirez
# Function: to monitor the state of a disk's path, switch, server, and node

# imports necessary packages
import subprocess
import os
import json
import sys
import shutil

# Nagios States
STATE_ACTIVE = 0
STATE_WARNING = 1
STATE_CRITICAL = 2
STATE_UNKNOWN = 3

# Sample multipath command outputs in python scripts
MULTIPATHD_PYTHON = "python multipathd.py show maps json"
PVS_PYTHON = "python pvs.py"


def main():
    # Initial exit_code
    exit_code = None

    # Standard output for multipathd
    multipath_output = ""

    # Initial values for the amount of data and how many are up and down
    map_total = 0
    map_up = 0
    map_down = 0
    path_group_total = 0
    path_group_up = 0
    path_group_down = 0
    path_total = 0
    path_up = 0
    path_down = 0

    # empty hashmap for all of the device's information
    device = {}

    try:
        # Checks program's existence - Uncheck when program is complete
        # open(MULTIPATHD_PYTHON)
        # open(PVS_PYTHON)

        # Receives python output for the multipath data
        multipathd_json = os.popen(MULTIPATHD_PYTHON).read()
        pvs_json = os.popen(PVS_PYTHON).read()

        # Converts outputs into JSON objects
        multipathd = json.loads(str(multipathd_json))
        pvs = json.loads(str(pvs_json))

        for maps in multipathd['maps']:
            map_name = device['map_name'] = maps['name']  
            device['maps_dm_st'] = maps['dm_st']
            map_total += 1
            map_up += 1
            if device['maps_dm_st'] != "active":
                map_up -= 1
                map_down += 1
                device['failed_map'] = device['map_name']
            for path_groups in maps['path_groups']:
                device['pg_dm_st'] = path_groups['dm_st']
                path_group_total += 1
                path_group_up += 1
                if device['pg_dm_st'] == "failed":
                    path_group_up -= 1
                    path_group_down += 1
                    device['failed_map'] = device['map_name']
                for path in path_groups['paths']:
                    device['dm_st'] = path['dm_st']
                    device['dev_st'] = path['dev_st']
                    device['chk_st'] = path['chk_st']

                    # increments total amount of paths and how many are up
                    path_total += 1
                    path_up += 1

                    # Determines the status of the device & decrements the paths up and increments paths down
                    if device['dm_st'] != "active" or device['dev_st'] != "running" or device['chk_st'] != "ready":
                        path_up -= 1
                        path_down += 1
                        device['failed_map'] = maps['name']
                        device['failed_dm_st'] = device['dm_st']
                        device['failed_dev_st'] = device['dev_st']
                        device['failed_chk_st'] = device['chk_st']

        # if no failure, message says how many path_groups there are and how many paths there are, up and down
        if path_up == path_total and map_down == 0 and dev_down == 0:
            multipath_output = "OK: {m_total} maps - {p_total}/{p_up}/{p_down} All paths up" \
                .format(m_total=device['map_total'], p_total=device['path_total'],
                        p_up=device['path_up'], p_down=device['path_down'])
            exit_code = STATE_ACTIVE

        # if a path has failed, open pvs output and looks for corresponding vg_name by searching for the failed map name
        elif path_down > 0 or dev_down > 0 or map_down > 0:
            for report in pvs['report']:
                for pv in report['pv']:
                    if device['failed_map'] in pv['pv_name']:
                        device['failed_vg_name'] = pv['vg_name']
            if map_down > 0 or path_down >= 0:
                multipath_output = "CRIT: {m_total} maps - {p_total}/{p_up}/{p_down} {f_vg_name} {f_map}/" \
                                   "{f_maps_up}/{f_maps_down}".format(m_total=map_total, p_total=path_total,
                                                                      p_up=path_up, p_down=path_down,
                                                                      f_vg_name=device['failed_vg_name'],
                                                                      f_map=device['failed_map'], f_maps_up=map_up,
                                                                      f_maps_down=map_down)
            exit_code = STATE_CRITICAL
            if path_down == 0:
                exit_code = STATE_WARNING
    except FileNotFoundError as file_err:
        exit_code = STATE_UNKNOWN
        multipath_output = "UNKNOWN: Multipath or PVS not found with {err}".format(err=str(file_err))

    # Prints multipath's path states & exits program with return code
    print(multipath_output)
    sys.exit(exit_code)


if __name__ == "__main__":
    main()

# INFO:
# state_warning - when any map has some but not all paths down
# state_critical - when any map has no paths up

# QUESTIONS: 
# Should I add a variable i.e. 'map_name' on line 63 to make the rest of the program easier to read or will variables
#       assigned to hashmaps ruin the function

# TODO:
# change to "CRIT: {map_total} maps - {path_total}/{path_up}/{path_down} {failed_vg_name} {failed_map_name}
#               {failed_map_paths}/{failed_map_paths_up}/{failed_map_paths_down}"
